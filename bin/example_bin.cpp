//
// Created by kewang on 4/15/16.
//

#define _USE_MATH_DEFINES

#include "orb_cuda.h"

#include "cuda.h"
#include "cuda_runtime.h"

#include "opencv2/core.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/calib3d.hpp"

#include "boost/thread.hpp"

#include <cmath>
#include <iostream>
#include <chrono>

cv::Mat LoadImage(const std::string& path, const int max_resolution) {
  cv::Mat image = cv::imread(path, CV_LOAD_IMAGE_GRAYSCALE);

  if (image.rows > max_resolution || image.cols > max_resolution) {
    const float factor =
      std::min(max_resolution / static_cast<float>(image.rows),
      max_resolution / static_cast<float>(image.cols));
    cv::resize(image, image,
      cv::Size(factor * image.cols, factor * image.rows), 0,
      0, cv::INTER_AREA);
  }

  return image;
}

void ExtractOrb(const cv::Mat& image, const int max_num_features, const int embedding_dimension,
        chitu::OrbCUDA* handle, std::vector<cv::KeyPoint>* keypoints,
        cv::Mat* descriptors, cv::Mat* descriptors_embedding) {
  std::vector<float> keypoints_buffer(max_num_features * 4);
  std::vector<uint8_t> descriptors_buffer(max_num_features * 32);
  std::vector<float> descriptors_embedding_buffer(max_num_features * embedding_dimension);

  handle->Detect(image.data, image.rows, image.cols);
  const int num_features = handle->Download(keypoints_buffer.data(), descriptors_buffer.data(), descriptors_embedding_buffer.data());

  keypoints->clear();
  keypoints->reserve(num_features);
  *descriptors = cv::Mat(num_features, 32, CV_8U, descriptors_buffer.data()).clone();
  *descriptors_embedding = cv::Mat(num_features, embedding_dimension, CV_32F, descriptors_embedding_buffer.data()).clone();
  for (int i = 0; i < num_features; ++i) {
    keypoints->emplace_back(keypoints_buffer[4 * i + 0], keypoints_buffer[4 * i + 1],
      keypoints_buffer[4 * i + 3], keypoints_buffer[4 * i + 2] / M_PI * 180);
  }
}

int main(int argc, char *argv[]) {
  if (argc != 6) {
    std::cout << "Usage: ./example_bin image1.jpg image2.jpg raw_matches.jpg inlier_matches.jpg" << std::endl;
    exit(EXIT_FAILURE);
  }

  chitu::OrbCUDAConfig cfg;
  cfg.gpu_id_ = 0;
  cfg.embedding_dimension_ = 32;

  chitu::OrbCUDA handle(cfg);
  if (handle.Init() != true) {
    exit(EXIT_FAILURE);
  }

  cv::Mat image1 = LoadImage(argv[1], cfg.maximum_resolution_);
  cv::Mat image2 = LoadImage(argv[2], cfg.maximum_resolution_);

  std::vector<cv::KeyPoint> keypoints1;
  cv::Mat descriptors1, descriptors_embedding1;
  //orb(image1, cv::noArray(), keypoints1, descriptors1);
  ExtractOrb(image1, cfg.maximum_feature_num_, cfg.embedding_dimension_, &handle, &keypoints1, &descriptors1, &descriptors_embedding1);

  std::vector<cv::KeyPoint> keypoints2;
  cv::Mat descriptors2, descriptors_embedding2;
  //orb(image2, cv::noArray(), keypoints2, descriptors2);
  ExtractOrb(image2, cfg.maximum_feature_num_, cfg.embedding_dimension_, &handle, &keypoints2, &descriptors2, &descriptors_embedding2);

  std::cout << keypoints1.size() << " " << keypoints2.size() << std::endl;

  std::cout << descriptors_embedding1.rows << " " << descriptors_embedding1.cols << std::endl;
  //std::cout << descriptors_embedding1 << std::endl;

  //cv::BFMatcher matcher(cv::NORM_HAMMING, true);
  //std::vector<cv::DMatch> matches;
  //matcher.match(descriptors1, descriptors2, matches);

  cv::BFMatcher matcher(cv::NORM_L2, true);
  std::vector<cv::DMatch> matches;
  matcher.match(descriptors_embedding1, descriptors_embedding2, matches);

  cv::Mat matches_image;
  cv::drawMatches(image1, keypoints1, image2, keypoints2, matches, matches_image);
  cv::imwrite(argv[3], matches_image);

  std::vector<cv::Point2f> points1;
  std::vector<cv::Point2f> points2;
  for (int i = 0; i < matches.size(); ++i) {
    points1.emplace_back(keypoints1[matches[i].queryIdx].pt);
    points2.emplace_back(keypoints2[matches[i].trainIdx].pt);
  }

  cv::Mat mask;
  cv::findFundamentalMat(points1, points2, mask, cv::FM_RANSAC, 2.0);

  cv::Mat matches_image2;
  cv::drawMatches(image1, keypoints1, image2, keypoints2, matches, matches_image2,
          cv::Scalar::all(-1), cv::Scalar::all(-1), mask);
  cv::imwrite(argv[4], matches_image2);

  std::cout << matches.size() << " " << cv::countNonZero(mask) << std::endl;

  cv::Mat keypoints_image;
  cv::drawKeypoints(image1, keypoints1, keypoints_image, cv::Scalar::all(-1),
    cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
  cv::imwrite(argv[5], keypoints_image);

  exit(EXIT_SUCCESS);
}
