//
// Created by kewang on 4/13/16.
//

#define BOOST_TEST_MODULE multi_thread_memcpy_speed_test

#include "cuda.h"
#include "cuda_runtime.h"

#include <chrono>
#include <iostream>
#include <random>
#include <vector>

#include <cstring>

#include "boost/thread.hpp"
#include "boost/log/trivial.hpp"
#include "boost/test/unit_test.hpp"

std::vector<char *> host_pageable_buffers;
std::vector<char *> host_pinned_buffers;
std::vector<char *> device_buffers;
std::vector<cudaStream_t> device_streams;
const int max_buffer_number = 12;
const int repeat = 100;
const size_t buffer_size = 640 * 480;

bool init() {

    host_pageable_buffers.resize(max_buffer_number);
    host_pinned_buffers.resize(max_buffer_number);
    device_buffers.resize(max_buffer_number);
    device_streams.resize(max_buffer_number);

    for (int buffer_index = 0; buffer_index < max_buffer_number; ++buffer_index) {
        host_pageable_buffers[buffer_index] = new char[buffer_size];
        cudaError_t err = cudaMallocHost(&(host_pinned_buffers[buffer_index]), buffer_size);
        BOOST_REQUIRE(err == cudaSuccess);
        err = cudaMalloc(&(device_buffers[buffer_index]), buffer_size);
        BOOST_REQUIRE(err == cudaSuccess);
        err = cudaStreamCreate(&(device_streams[buffer_index]));
        BOOST_REQUIRE(err == cudaSuccess);
    }

    return true;
}

void test_host_memcpy(int buffer_index) {
    for (int test_index = 0; test_index < repeat; ++test_index) {
        std::memcpy(host_pageable_buffers[buffer_index], host_pinned_buffers[buffer_index], buffer_size);
    }
}

void test_sync_memcpy(int buffer_index) {
    for (int test_index = 0; test_index < repeat; ++test_index) {
        cudaMemcpy(device_buffers[buffer_index], host_pageable_buffers[buffer_index], buffer_size,
                   cudaMemcpyHostToDevice);
    }
}

void test_async_memcpy(int buffer_index) {
    for (int test_index = 0; test_index < repeat; ++test_index) {
        cudaMemcpyAsync(device_buffers[buffer_index], host_pinned_buffers[buffer_index], buffer_size,
                        cudaMemcpyHostToDevice, device_streams[buffer_index]);
    }
}

void test_async_memcpy_with_host_copy(int buffer_index) {
    for (int test_index = 0; test_index < repeat; ++test_index) {
        std::memcpy(host_pageable_buffers[buffer_index], host_pinned_buffers[buffer_index], buffer_size);
        cudaMemcpyAsync(device_buffers[buffer_index], host_pinned_buffers[buffer_index], buffer_size,
                        cudaMemcpyHostToDevice, device_streams[buffer_index]);
    }
}


BOOST_AUTO_TEST_SUITE(memcpy_test_suite)

    BOOST_AUTO_TEST_CASE(test_host) {
        init();
        boost::thread_group threads;
        std::chrono::time_point<std::chrono::high_resolution_clock> start;
        std::chrono::time_point<std::chrono::high_resolution_clock> end;
        for (int total_thread_num = 1; total_thread_num <= max_buffer_number; ++total_thread_num) {
            start = std::chrono::high_resolution_clock::now();
            for (int current_thread_index = 0; current_thread_index < total_thread_num; ++current_thread_index) {
                threads.add_thread(new boost::thread(test_host_memcpy, current_thread_index));
            }
            threads.join_all();
            end = std::chrono::high_resolution_clock::now();
            auto elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
            double avg_time = elapsed_time.count() / (total_thread_num * repeat);
            double speed = 1e9 / (elapsed_time.count() / (total_thread_num * repeat));
            BOOST_LOG_TRIVIAL(info) << "HOST memcpy (pageable->pinned) thread#:" << total_thread_num << " " <<
                                    avg_time << " ns. " << speed << " Hz.";
        }
    }

    BOOST_AUTO_TEST_CASE(test_sync) {
        init();
        boost::thread_group threads;
        std::chrono::time_point<std::chrono::high_resolution_clock> start;
        std::chrono::time_point<std::chrono::high_resolution_clock> end;
        for (int total_thread_num = 1; total_thread_num <= max_buffer_number; ++total_thread_num) {
            start = std::chrono::high_resolution_clock::now();
            for (int current_thread_index = 0; current_thread_index < total_thread_num; ++current_thread_index) {
                threads.add_thread(new boost::thread(test_sync_memcpy, current_thread_index));
            }
            cudaDeviceSynchronize();
            threads.join_all();
            end = std::chrono::high_resolution_clock::now();
            auto elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
            double avg_time = elapsed_time.count() / (total_thread_num * repeat);
            double speed = 1e9 / (elapsed_time.count() / (total_thread_num * repeat));
            BOOST_LOG_TRIVIAL(info) << "CUDA memcpy sync thread#:" << total_thread_num << " " << avg_time << " ns. " <<
                                    speed << " Hz.";
        }
    }

    BOOST_AUTO_TEST_CASE(test_async) {
        init();
        boost::thread_group threads;
        std::chrono::time_point<std::chrono::high_resolution_clock> start;
        std::chrono::time_point<std::chrono::high_resolution_clock> end;
        for (int total_thread_num = 1; total_thread_num <= max_buffer_number; ++total_thread_num) {
            start = std::chrono::high_resolution_clock::now();
            for (int current_thread_index = 0; current_thread_index < total_thread_num; ++current_thread_index) {
                threads.add_thread(new boost::thread(test_async_memcpy, current_thread_index));
            }
            threads.join_all();
            cudaDeviceSynchronize();
            end = std::chrono::high_resolution_clock::now();
            auto elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
            double avg_time = elapsed_time.count() / (total_thread_num * repeat);
            double speed = 1e9 / (elapsed_time.count() / (total_thread_num * repeat));
            BOOST_LOG_TRIVIAL(info) << "CUDA memcpy async thread#:" << total_thread_num << " " << avg_time << " ns. " <<
                                    speed << " Hz.";
        }
    }

    BOOST_AUTO_TEST_CASE(test_async_with_host_copy) {
        init();
        boost::thread_group threads;
        std::chrono::time_point<std::chrono::high_resolution_clock> start;
        std::chrono::time_point<std::chrono::high_resolution_clock> end;
        for (int total_thread_num = 1; total_thread_num <= max_buffer_number; ++total_thread_num) {
            start = std::chrono::high_resolution_clock::now();
            for (int current_thread_index = 0; current_thread_index < total_thread_num; ++current_thread_index) {
                threads.add_thread(new boost::thread(test_async_memcpy_with_host_copy, current_thread_index));
            }
            threads.join_all();
            cudaDeviceSynchronize();
            end = std::chrono::high_resolution_clock::now();
            auto elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
            double avg_time = elapsed_time.count() / (total_thread_num * repeat);
            double speed = 1e9 / (elapsed_time.count() / (total_thread_num * repeat));
            BOOST_LOG_TRIVIAL(info) << "CUDA memcpy async + host thread#:" << total_thread_num << " " << avg_time <<
                                    " ns. " << speed << " Hz.";
        }
    }

BOOST_AUTO_TEST_SUITE_END()
