//
// Created by kewang on 4/15/16.
//

#ifndef CHITUMT_ORB_CUDA_H
#define CHITUMT_ORB_CUDA_H

#include "cuda.h"
#include "cuda_runtime.h"

#include <iostream>
#include <fstream>
#include <vector>

#include <cstdint>
#include <cstdlib>

#define CUDA_CALL(x)                                              \
    do {                                                          \
        if ((x) != cudaSuccess) {                                 \
            printf("Error %d:%s at %s:%d\n", x, cudaGetErrorString(x), __FILE__, __LINE__); \
            exit(EXIT_FAILURE);                                   \
        }                                                         \
    } while (0)

#define CUDA_CHECK_ERRORS() \
    do { \
        cudaError_t __err = cudaGetLastError(); \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s at %s:%d\n", \
                cudaGetErrorString(__err), \
                __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(1); \
                } \
        } while (0)

namespace chitu {
    // GPUPitchedImageBuffer stores image in row-major. Image is strided for efficient access.
    template<typename T>
    struct GPUPitchedImageBuffer {
        size_t pitch_ = 0; // row size in bytes
        int max_height_ = 0;
        int max_width_ = 0;
        int height_ = 0;
        int width_ = 0;
        size_t stride_ = 0; // row size in elements
        T *buffer_ = nullptr;

        bool Allocate(int height, int width) {
            if (buffer_ != nullptr) {
                CUDA_CALL(cudaFree(buffer_));
            }
            CUDA_CALL(cudaMallocPitch(&(buffer_), &(pitch_), width * sizeof(T), height));
            max_width_ = width;
            width_ = width;
            max_height_ = height;
            height_ = height;
            stride_ = pitch_ / sizeof(T);

            return true;
        }

        void Release() {
            if (buffer_ != nullptr) {
                CUDA_CALL(cudaFree(buffer_));
            }
        }

        // copy image from host buffer to GPU memory
        bool Upload(T *h_buffer, int height, int width, cudaStream_t stream = 0) {
            if (height > max_height_ || width > max_width_) {
                return false;
            }
            height_ = height;
            width_ = width;

            CUDA_CALL(cudaMemcpy2DAsync(buffer_, pitch_, h_buffer, sizeof(T) * width, sizeof(T) * width, height,
                                        cudaMemcpyHostToDevice, stream));

            return true;
        }

        // output host buffer to file for DEBUG purpose
        // DO NOT use in production code
        void OutputChar(std::string filename) {
            std::ofstream out_file(filename);
            std::vector<T> host_buffer(stride_ * height_);
            CUDA_CALL(cudaMemcpy2D(host_buffer.data(), pitch_, buffer_, pitch_,
                                   pitch_, height_, cudaMemcpyDeviceToHost));

            for (int row = 0; row < height_; ++row) {
                for (int col = 0; col < width_; ++col) {
                    out_file << static_cast<int>(host_buffer[col + row * stride_]) << " ";
                }
                out_file << std::endl;
            }
            out_file << std::endl;
            out_file.close();
        }

        void Output(std::string filename) {
            std::ofstream out_file(filename);
            std::vector<T> host_buffer(stride_ * height_);
            CUDA_CALL(cudaMemcpy2D(host_buffer.data(), pitch_, buffer_, pitch_,
                                   pitch_, height_, cudaMemcpyDeviceToHost));

            for (int row = 0; row < height_; ++row) {
                for (int col = 0; col < width_; ++col) {
                    out_file << host_buffer[col + row * stride_] << " ";
                }
                out_file << std::endl;
            }
            out_file << std::endl;
            out_file.close();
        }
    };

    template<typename T>
    struct GPULinearImageBuffer {
        int height_ = 0;
        int width_ = 0;
        int max_height_ = 0;
        int max_width_ = 0;
        T *buffer_ = nullptr;

        bool Allocate(int height, int width) {
            if (buffer_ != nullptr) {
                CUDA_CALL(cudaFree(buffer_));
            }
            CUDA_CALL(cudaMalloc(&(buffer_), width * sizeof(T) * height));
            width_ = width;
            max_height_ = height;
            height_ = height;
            max_width_ = width;

            return true;
        }

        void Release() {
            if (buffer_ != nullptr) {
                CUDA_CALL(cudaFree(buffer_));
            }
        }

        void OutputChar(std::string filename) {
            std::ofstream out_file(filename);
            std::vector<T> host_buffer(width_ * height_);
            CUDA_CALL(cudaMemcpy(host_buffer.data(), buffer_, width_ * height_ * sizeof(T), cudaMemcpyDeviceToHost));

            for (int row = 0; row < height_; ++row) {
                for (int col = 0; col < width_; ++col) {
                    out_file << static_cast<int>(host_buffer[col + row * width_]) << " ";
                }
                out_file << std::endl;
            }
            out_file << std::endl;
            out_file.close();
        }

        void Output(std::string filename) {
            std::ofstream out_file(filename);
            std::vector<T> host_buffer(width_ * height_);
            CUDA_CALL(cudaMemcpy(host_buffer.data(), buffer_, width_ * height_ * sizeof(T), cudaMemcpyDeviceToHost));

            for (int row = 0; row < height_; ++row) {
                for (int col = 0; col < width_; ++col) {
                    out_file << host_buffer[col + row * width_] << " ";
                }
                out_file << std::endl;
            }
            out_file << std::endl;
            out_file.close();
        }
    };

    using GPULinearBufferFloat = GPULinearImageBuffer<float>;
    using GPULinearBufferInt = GPULinearImageBuffer<int>;
    using GPULinearBufferUChar= GPULinearImageBuffer<unsigned char>;
    using GPUPitchedBufferFloat = GPUPitchedImageBuffer<float>;
    using GPUPitchedBufferInt = GPUPitchedImageBuffer<int>;
    using GPUPitchedBufferUChar = GPUPitchedImageBuffer<unsigned char>;

    struct OrbCUDAConfig {
        int gpu_id_ = 0;
        int maximum_resolution_ = 1024;
        int maximum_feature_num_ = 4096;
        int embedding_dimension_ = 32;
        int border_threshold_ = 31;
        int n_levels_ = 8;
        float downfactor_ = 1.20f;
        float harris_threshold_ = 0.04f;
        unsigned char fast_threshold_ = 20;
    };

    class OrbCUDA {
    public:
        OrbCUDA(const OrbCUDAConfig& cfg) : cfg_(cfg) {}

        ~OrbCUDA() {
            CUDA_CALL(cudaStreamDestroy(stream_));
            if (h_descriptors_embedding_ != nullptr) {
              CUDA_CALL(cudaFreeHost(h_descriptors_embedding_));
            }
            if (d_descriptors_embedding_ != nullptr) {
              CUDA_CALL(cudaFree(d_descriptors_));
            }
            if (h_pinned_desc_ != nullptr) {
              CUDA_CALL(cudaFreeHost(h_pinned_desc_));
            }
            if (d_descriptors_ != nullptr) {
              CUDA_CALL(cudaFree(d_descriptors_));
            }
            if (d_keypoints_ != nullptr) {
              CUDA_CALL(cudaFree(d_keypoints_));
            }
            if (h_pinned_kpts_ != nullptr) {
                CUDA_CALL(cudaFreeHost(h_pinned_kpts_));
            }
            if (h_feature_number_ != nullptr) {
                CUDA_CALL(cudaFreeHost(h_feature_number_));
            }
            if (d_feature_number_ != nullptr) {
                CUDA_CALL(cudaFree(d_feature_number_));
            }
            if (h_pinned_buffer_ != nullptr) {
                CUDA_CALL(cudaFreeHost(h_pinned_buffer_));
            }
            if (d_prescan_buffer_ != nullptr) {
                CUDA_CALL(cudaFree(d_prescan_buffer_));
            }
            if (d_sort_buffer_ != nullptr) {
                CUDA_CALL(cudaFree(d_sort_buffer_));
            }
            CUDA_CALL(cudaDestroyTextureObject(proj_matrix_tex_));
            sorted_compact_score_.Release();
            compact_score_.Release();
            sorted_compact_indices_.Release();
            compact_indices_.Release();
            keypoint_indices_.Release();
            keypoint_flag_.Release();
            proj_matrix_.Release();
            float_buffer_.Release();
            smooth_buffer_.Release();
            low_res_image_buffer_.Release();
            image_buffer_.Release();
        }

        bool Init();

        void Detect(const unsigned char *h_pageable_buffer, int height, int width, bool upright = false);

        int Download(float *h_kpts, uint8_t *h_desc, float* h_embedding);

    private:
        OrbCUDA(const OrbCUDA&) = delete;
        OrbCUDA& operator=(const OrbCUDA&) = delete;
        OrbCUDA(const OrbCUDA&&) = delete;
        OrbCUDA& operator=(const OrbCUDA&&) = delete;

        OrbCUDAConfig cfg_;

        GPUPitchedBufferUChar image_buffer_;
        GPUPitchedBufferUChar low_res_image_buffer_;
        GPUPitchedBufferUChar smooth_buffer_;
        GPUPitchedBufferFloat float_buffer_; // used for gaussian smooth, fast corner score, harris score
        GPUPitchedBufferFloat proj_matrix_;
        GPULinearBufferInt keypoint_flag_;
        GPULinearBufferInt keypoint_indices_;
        GPULinearBufferInt compact_indices_;
        GPULinearBufferInt sorted_compact_indices_;
        GPULinearBufferFloat compact_score_;
        GPULinearBufferFloat sorted_compact_score_;

        cudaTextureObject_t proj_matrix_tex_;

        // prescan buffer
        void *d_prescan_buffer_ = nullptr;
        size_t d_prescan_buffer_size_ = 0;
        // radix sort buffer
        void *d_sort_buffer_ = nullptr;
        size_t d_sort_buffer_size_ = 0;

        // host pinned image buffer
        unsigned char *h_pinned_buffer_;

        // device feature number
        unsigned int *d_feature_number_;
        unsigned int *h_feature_number_;
        unsigned int h_feature_offset_;
        std::vector<unsigned int> h_octave_max_feature_numbers_;
        // keypoint buffer
        float4 *d_keypoints_;
        float4 *h_pinned_kpts_;
        // descriptor buffer
        uint8_t *d_descriptors_;
        uint8_t *h_pinned_desc_;
        float* d_descriptors_embedding_;
        float* h_descriptors_embedding_;

        cudaStream_t stream_;
    };

} // namespace chitu
#endif //CHITUMT_ORB_CUDA_H
