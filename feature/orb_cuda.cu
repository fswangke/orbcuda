#include "orb_cuda.h"

#include "cuda.h"
#include "cuda_runtime.h"
#include "driver_types.h"

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cstdint>

#ifdef _WIN32
#include <algorithm>
#endif

#include "cub.cuh"
#include "device/device_radix_sort.cuh"
#include "device/device_scan.cuh"
#include "util_allocator.cuh"


namespace chitu {
	inline int iDivUp(int a, int b) {
		return (a % b != 0) ? (a / b + 1) : (a / b);
	}

  inline int iAlignUp(int a, int b) {
    return (a % b != 0) ? (a - a % b + b) : a;
  }

	__constant__ float weight[5] = {
		0.000263865082737f, 0.106450771973592f, 0.786570725887342f, 0.106450771973592f, 0.000263865082737f };
	__constant__ unsigned char fast_corner_compare_lut[512];
	__constant__ char fast_corner_col_offset[] = { 0, 1, 2, 3, 3, 3, 2, 1, 0, -1, -2, -3, -3, -3, -2, -1 };
	__constant__ char fast_corner_row_offset[] = { 3, 3, 2, 1, 0, -1, -2, -3, -3, -3, -2, -1, 0, 1, 2, 3 };
	//__constant__ char fast_corner_row_offset[] = { -3, -3, -2, -1, 0, 1, 2, 3, 3, 3, 2, 1, 0, -1, -2, -3 };

#include "orb_pattern.h"
#include "proj_matrix.h"

	template<int i>
	__device__ inline float ConvolveRow(cudaTextureObject_t image_tex, int row, int col) {
		return tex2D<float>(image_tex, col + 2 - i, row) * weight[i] + ConvolveRow<i - 1>(image_tex, row, col);
	}

	template<>
	__device__ inline float ConvolveRow<-1>(cudaTextureObject_t image_tex, int row, int col) {
		return 0.0f;
	}

	template<int i>
	__device__ inline float ConvolveCol(cudaTextureObject_t image_tex, int row, int col) {
		return tex2D<float>(image_tex, col, row + 2 - i) * weight[i] + ConvolveCol<i - 1>(image_tex, row, col);
	}

	template<>
	__device__ inline float ConvolveCol<-1>(cudaTextureObject_t image_tex, int row, int col) {
		return 0.0f;
	}

	__global__ void GaussianBlurRowKernel(cudaTextureObject_t image_tex, GPUPitchedBufferFloat buf) {
		const int row = blockDim.x * blockIdx.x + threadIdx.x;
		const int col = blockDim.y * blockIdx.y + threadIdx.y;
		if (row >= buf.height_ || col >= buf.width_) {
			return;
		}

		buf.buffer_[col + row * buf.stride_] = ConvolveRow<4>(image_tex, row, col);
	}

	__global__ void GaussianBlurColKernel(cudaTextureObject_t fp_tex, GPUPitchedBufferUChar image) {
		const int row = blockDim.x * blockIdx.x + threadIdx.x;
		const int col = blockDim.y * blockIdx.y + threadIdx.y;
		if (row >= image.height_ || col >= image.width_) {
			return;
		}

		image.buffer_[col + row * image.stride_] = ConvolveCol<4>(fp_tex, row, col) * 255.0f + 0.5f;
	}

	__global__ void DownsampleKernel(cudaTextureObject_t image_tex, GPUPitchedBufferUChar downsampled_image) {
		const int row = blockDim.x * blockIdx.x + threadIdx.x;
		const int col = blockDim.y * blockIdx.y + threadIdx.y;
		if (row >= downsampled_image.height_ || col >= downsampled_image.width_) {
			return;
		}
		const float fp_row = (row + 0.5f) / downsampled_image.height_;
		const float fp_col = (col + 0.5f) / downsampled_image.width_;

		downsampled_image.buffer_[col + row * downsampled_image.stride_] =
			tex2D<float>(image_tex, fp_col, fp_row) * 255.0f + 0.5;
	}
	__device__ __forceinline__ unsigned char GetFastNeighbor(cudaTextureObject_t image_tex, int row, int col,
		int offset) {
		return tex2D<unsigned char>(image_tex, col + fast_corner_col_offset[offset],
			row + fast_corner_row_offset[offset]);
	}

	__global__ void Fast9CornerDetectionKernel(cudaTextureObject_t image_tex,
		const unsigned char threshold, const int border_threshold,
		GPUPitchedBufferFloat corner_score) {
		const int row = blockDim.x * blockIdx.x + threadIdx.x;
		const int col = blockDim.y * blockIdx.y + threadIdx.y;
		const int height = corner_score.height_;
		const int width = corner_score.width_;
		if (row >= height || col >= width) {
			return;
		}

		if (row < border_threshold || row >= height - border_threshold || col < border_threshold ||
			col >= width - border_threshold) {
			corner_score.buffer_[col + row * corner_score.stride_] = 0;
			return;
		}

		unsigned char center_pixel = tex2D<unsigned char>(image_tex, col, row);
		unsigned char *current_compare_lut = fast_corner_compare_lut - center_pixel + 255;
		unsigned char d =
			current_compare_lut[GetFastNeighbor(image_tex, row, col, 0)] |
			current_compare_lut[GetFastNeighbor(image_tex, row, col, 8)];
		if (d == 0) {
			corner_score.buffer_[col + row * corner_score.stride_] = 0;
			return;
		}
		d &= current_compare_lut[GetFastNeighbor(image_tex, row, col, 2)] |
			current_compare_lut[GetFastNeighbor(image_tex, row, col, 10)];
		d &= current_compare_lut[GetFastNeighbor(image_tex, row, col, 4)] |
			current_compare_lut[GetFastNeighbor(image_tex, row, col, 12)];
		d &= current_compare_lut[GetFastNeighbor(image_tex, row, col, 6)] |
			current_compare_lut[GetFastNeighbor(image_tex, row, col, 14)];
		if (d == 0) {
			corner_score.buffer_[col + row * corner_score.stride_] = 0;
			return;
		}
		d &= current_compare_lut[GetFastNeighbor(image_tex, row, col, 1)] |
			current_compare_lut[GetFastNeighbor(image_tex, row, col, 9)];
		d &= current_compare_lut[GetFastNeighbor(image_tex, row, col, 3)] |
			current_compare_lut[GetFastNeighbor(image_tex, row, col, 11)];
		d &= current_compare_lut[GetFastNeighbor(image_tex, row, col, 5)] |
			current_compare_lut[GetFastNeighbor(image_tex, row, col, 13)];
		d &= current_compare_lut[GetFastNeighbor(image_tex, row, col, 7)] |
			current_compare_lut[GetFastNeighbor(image_tex, row, col, 15)];
		if (d == 0) {
			corner_score.buffer_[col + row * corner_score.stride_] = 0;
			return;
		}

		int vt = 0;
		int count = 0;
		if (d & 1) {
			// lower
			vt = center_pixel - threshold;
			for (int l = 0; l < 25; ++l) {
				unsigned char neighbor = GetFastNeighbor(image_tex, row, col, l & 0x0000000F);
				if (neighbor < vt) {
					count++;
					if (count >= 9) {
						break;
					}
				}
				else {
					count = 0;
				}
			}
		}
		else if (d & 2) {
			// higher
			vt = center_pixel + threshold;
			for (int l = 0; l < 25; ++l) {
				unsigned char neighbor = GetFastNeighbor(image_tex, row, col, l & 0x0000000F);
				if (neighbor > vt) {
					count++;
					if (count >= 9) {
						break;
					}
				}
				else {
					count = 0;
				}
			}
		}

		if (count < 9) {
			corner_score.buffer_[col + row * corner_score.stride_] = 0;
			return;
		}

		float score = 0.0f;
#pragma unroll
		for (int m = 0; m < 16; ++m) {
			score += abs((int)GetFastNeighbor(image_tex, row, col, m) - (int)center_pixel);
		}

		corner_score.buffer_[col + row * corner_score.stride_] = score;
	}

	__device__ __forceinline__ int IsLocalMaximum(cudaTextureObject_t fast_score_tex, int row, int col) {
		const float center_value = tex2D<float>(fast_score_tex, col, row);
		const int kSuppressionRadius = 1;
		// TODO: manual unroll
		for (int r = -kSuppressionRadius; r <= kSuppressionRadius; ++r) {
			for (int c = -kSuppressionRadius; c <= kSuppressionRadius; ++c) {
				if (r == 0 && c == 0) {
					continue;
				}
				if (center_value <= tex2D<float>(fast_score_tex, col + c, row + r)) {
					return 0;
				}
			}
		}

		return 1;
	}

	__global__ void NonMaximumSuppressionKernel(cudaTextureObject_t fast_score_tex, GPULinearBufferInt fast_flag, const int border_threshold = 3) {
		const int row = blockDim.x * blockIdx.x + threadIdx.x;
		const int col = blockDim.y * blockIdx.y + threadIdx.y;
		if (row >= fast_flag.height_ || col >= fast_flag.width_) {
			return;
		}
		if (row < border_threshold || row >= fast_flag.height_ - border_threshold || col < border_threshold ||
			col >= fast_flag.width_ - border_threshold) {
			fast_flag.buffer_[col + row * fast_flag.width_] = 0;
			return;
		}

		fast_flag.buffer_[col + row * fast_flag.width_] = IsLocalMaximum(fast_score_tex, row, col);
	}

	__device__ __forceinline__ float ComputeHarrisCornernessScore(cudaTextureObject_t image_tex, int center_row,
		int center_col, const float harris_k = 0.04f) {
		float sum_dxdx = 0.0f;
		float sum_dydy = 0.0f;
		float sum_dxdy = 0.0f;

		float center_col_mid = center_col + 0.5f;
		float center_row_mid = center_row + 0.5f;

		// accumulate values
		const int kHarrisCornerRadius = 3;
		for (int row = -kHarrisCornerRadius; row <= kHarrisCornerRadius; ++row) {
			for (int col = -kHarrisCornerRadius; col <= kHarrisCornerRadius; ++col) {
				if (col * col + row * row > kHarrisCornerRadius * kHarrisCornerRadius) { continue; }
				// Use sobel kernel
				// Gx = [-1, 0, 1; Gy = [-1, -2, -1;
				//       -2, 0, 2;        0,  0, 0;
				//       -1, 0, 1]        1,  2, 1]
				float dx = (tex2D<float>(image_tex, center_col_mid + col + 1, center_row_mid + row - 1)
					- tex2D<float>(image_tex, center_col_mid + col - 1, center_row_mid + row - 1))
					+ 2 * (tex2D<float>(image_tex, center_col_mid + col + 1, center_row_mid + row)
					- tex2D<float>(image_tex, center_col_mid + col - 1, center_row_mid + row))
					+ (tex2D<float>(image_tex, center_col_mid + col + 1, center_row_mid + row + 1)
					- tex2D<float>(image_tex, center_col_mid + col - 1, center_row_mid + row + 1));
				float dy = (tex2D<float>(image_tex, center_col_mid + col - 1, center_row_mid + row + 1)
					- tex2D<float>(image_tex, center_col_mid + col - 1, center_row_mid + row - 1))
					+ 2 * (tex2D<float>(image_tex, center_col_mid + col, center_row_mid + row + 1)
					- tex2D<float>(image_tex, center_col_mid + col, center_row_mid + row - 1))
					+ (tex2D<float>(image_tex, center_col_mid + col + 1, center_row_mid + row + 1)
					- tex2D<float>(image_tex, center_col_mid + col + 1, center_row_mid + row - 1));
				sum_dxdx += dx * dx;
				sum_dydy += dy * dy;
				sum_dxdy += dx * dy;
			}
		}

		float sum_dxdx_dydy = sum_dxdx + sum_dydy;
		return sum_dxdx * sum_dydy - sum_dxdy * sum_dxdy - harris_k * sum_dxdx_dydy * sum_dxdx_dydy;
	}

	__global__ void CompactKeypointsKernel(GPULinearBufferInt d_compact_indices,
		GPULinearBufferInt d_fast_indices,
		GPULinearBufferInt d_fast_flag,
		unsigned int *total_count) {
		const int row = blockDim.x * blockIdx.x + threadIdx.x;
		const int col = blockDim.y * blockIdx.y + threadIdx.y;
		if (row >= d_fast_flag.height_ || col >= d_fast_flag.width_) {
			return;
		}

		const int index = col + row * d_fast_flag.width_;
		int flag = d_fast_flag.buffer_[index];
		if (flag == 1) {
			int current_kpt_index = d_fast_indices.buffer_[index] - 1;
			d_compact_indices.buffer_[current_kpt_index] = index;
		}

		if (index == d_fast_flag.height_ * d_fast_flag.width_ - 1) {
			*total_count = d_fast_indices.buffer_[index];
		}
	}

	__global__ void ComputeHarrisScorenessKernel(cudaTextureObject_t image_tex,
		GPULinearBufferInt d_compact_indices,
		float * d_compact_score,
		const int keypoint_number,
		const float harris_k = 0.04f) {
		const int index = blockDim.x * blockIdx.x + threadIdx.x;
		if (index >= keypoint_number) {
			return;
		}
		int corner_index = d_compact_indices.buffer_[index];
		int row = corner_index / d_compact_indices.width_;
		int col = corner_index % d_compact_indices.width_;
		d_compact_score[index] = ComputeHarrisCornernessScore(image_tex, row, col, harris_k);
	}

	__global__ void ComputeOrientation(cudaTextureObject_t image_tex, GPULinearImageBuffer<int> d_sorted_compact_indices,
		float4 *d_kpt, unsigned int feature_number, const float kpt_scale_x, const float kpt_scale_y) {
		int i = blockDim.x * blockIdx.x + threadIdx.x;
		if (i >= feature_number) {
			return;
		}

		int kpt_index = d_sorted_compact_indices.buffer_[i];
		float row = (kpt_index / d_sorted_compact_indices.width_) + 0.5f;
		float col = (kpt_index % d_sorted_compact_indices.width_) + 0.5f;

		float4 curr_kpt;
		curr_kpt.x = col;
		curr_kpt.y = row;

		float m01 = 0.0f;
		float m10 = 0.0f;
		const int kOrientationWinRadius = 15;
		for (int r = -kOrientationWinRadius; r <= kOrientationWinRadius; ++r) {
			size_t row_squqred = r * r;
			float temp_m01 = 0.0f;
			for (int c = -kOrientationWinRadius; c <= kOrientationWinRadius; ++c) {
				if (row_squqred + c * c > kOrientationWinRadius * kOrientationWinRadius) {
					continue;
				}
				// NOTICE: y corresponces to row, x corresponds to col
				float val = tex2D<float>(image_tex, c + curr_kpt.x, r + curr_kpt.y);
				m10 += c * val;
				temp_m01 += val;
			}
			m01 += r * temp_m01;
		}

		// NOTE: benchmark shows that atan2f is the major performance bottleneck of this kernel
		curr_kpt.z = atan2f(m01, m10);
		curr_kpt.w = 31.0f * 0.5f * (kpt_scale_x + kpt_scale_y);
		curr_kpt.x = curr_kpt.x * kpt_scale_x - 0.5f;
		curr_kpt.y = curr_kpt.y * kpt_scale_y - 0.5f;
		d_kpt[i] = curr_kpt;
	}

	__global__ void ExtractOrbDescriptor(
      cudaTextureObject_t image_tex, cudaTextureObject_t proj_matrix_tex,
      GPULinearImageBuffer<int> d_sorted_compact_indices,
      float4 *d_kpt, uint8_t *d_desc, float* d_desc_embedding,
      unsigned int feature_number, const int embedding_dimension) {
		int kpt_idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (kpt_idx >= feature_number) {
			return;
		}

    int kpt_linear_coord = d_sorted_compact_indices.buffer_[kpt_idx];
    float row = (kpt_linear_coord / d_sorted_compact_indices.width_) + 0.5f;
    float col = (kpt_linear_coord % d_sorted_compact_indices.width_) + 0.5f;

    float4 curr_kpt = d_kpt[kpt_idx];
		float sin_a = 0.0f;
		float cos_a = 0.0f;
		__sincosf(curr_kpt.z, &sin_a, &cos_a);

    // The memory locations of the current keypoint descriptors.
    uint8_t* kpt_desc = &d_desc[kpt_idx * 32];
    float* kpt_desc_embedding = &d_desc_embedding[kpt_idx * embedding_dimension];

    // Initialize the Euclidean embedding vector to zero.
    for (int i = 0; i < embedding_dimension; ++i) {
      kpt_desc_embedding[i] = 0.0f;
    }

		for (int word_index = 0; word_index < 32; ++word_index) {
      int word_offset = word_index * 8;
      int pattern_offset = word_offset * 4;
      uint8_t desc_word = 0;
			for (int bit = 0; bit < 8; ++bit) {
        const float pc0 = orb_compare_pattern[pattern_offset + bit * 4 + 0];
        const float pr0 = orb_compare_pattern[pattern_offset + bit * 4 + 1];
        const float pc1 = orb_compare_pattern[pattern_offset + bit * 4 + 2];
        const float pr1 = orb_compare_pattern[pattern_offset + bit * 4 + 3];
				float spc0 = (pc0 * cos_a - pr0 * sin_a);
				float spr0 = (pc0 * sin_a + pr0 * cos_a);
				float spc1 = (pc1 * cos_a - pr1 * sin_a);
				float spr1 = (pc1 * sin_a + pr1 * cos_a);
				float val1 = tex2D<float>(image_tex, col + spc0, row + spr0);
				float val2 = tex2D<float>(image_tex, col + spc1, row + spr1);
        uint8_t val = val1 < val2;
        desc_word |= val << bit;
        if (val) {
          // Accumulate the Euclidean embedding vector.
          for (int i = 0; i < embedding_dimension; ++i) {
            kpt_desc_embedding[i] += tex2D<float>(proj_matrix_tex, word_offset + bit, i);
          }
        }
			}
      kpt_desc[word_index] = desc_word;
		}
	}

	__global__ void ExtractOrbDescriptorUpright(
      cudaTextureObject_t image_tex, cudaTextureObject_t proj_matrix_tex, 
      GPULinearImageBuffer<int> d_sorted_compact_indices,
      float4 *d_kpt, uint8_t *d_desc, float* d_desc_embedding,
      unsigned int feature_number, const int embedding_dimension,
      const float kpt_scale_x, float kpt_scale_y) {
		int kpt_idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (kpt_idx >= feature_number) {
			return;
		}

    int kpt_linear_coord = d_sorted_compact_indices.buffer_[kpt_idx];
    float row = kpt_linear_coord / d_sorted_compact_indices.width_ + 0.5f;
    float col = kpt_linear_coord % d_sorted_compact_indices.width_ + 0.5f;
		
		float4 curr_kpt;
		curr_kpt.x = col * kpt_scale_x - 0.5f;
		curr_kpt.y = row * kpt_scale_y - 0.5f;
		curr_kpt.w = 31.0f * 0.5f * (kpt_scale_x + kpt_scale_y);
		curr_kpt.z = 0.0f;
    d_kpt[kpt_idx] = curr_kpt;

    // The memory locations of the current keypoint descriptors.
    uint8_t* kpt_desc = &d_desc[kpt_idx * 32];
    float* kpt_desc_embedding = &d_desc_embedding[kpt_idx * embedding_dimension];

    // Initialize the Euclidean embedding vector to zero.
    for (int i = 0; i < embedding_dimension; ++i) {
      kpt_desc_embedding[i] = 0.0f;
    }

		for (int word_index = 0; word_index < 32; ++word_index) {
      int word_offset = word_index * 8;
      int pattern_offset = word_offset * 4;
			uint8_t desc_word = 0;
			for (int bit = 0; bit < 8; ++bit) {
        const int pc0 = orb_compare_pattern[pattern_offset + bit * 4 + 0];
        const int pr0 = orb_compare_pattern[pattern_offset + bit * 4 + 1];
        const int pc1 = orb_compare_pattern[pattern_offset + bit * 4 + 2];
        const int pr1 = orb_compare_pattern[pattern_offset + bit * 4 + 3];
				float val1 = tex2D<float>(image_tex, col + pc0, row + pr0);
				float val2 = tex2D<float>(image_tex, col + pc1, row + pr1);
        uint8_t val = val1 < val2;
        desc_word |= val << bit;
        if (val) {
          // Accumulate the Euclidean embedding vector.
          for (int i = 0; i < embedding_dimension; ++i) {
            kpt_desc_embedding[i] += tex2D<float>(proj_matrix_tex, word_offset + bit, i);
          }
        }
			}
      kpt_desc[word_index] = desc_word;
		}
	}

	void OrbCUDA::Detect(const unsigned char *h_pageable_buffer, int height, int width, bool upright) {
		std::memcpy(h_pinned_buffer_, h_pageable_buffer, sizeof(unsigned char) * height * width);
		image_buffer_.Upload(h_pinned_buffer_, height, width, stream_);
		h_feature_offset_ = 0;
		*h_feature_number_ = 0;

		std::vector<int> level_widths(cfg_.n_levels_);
    std::vector<int> level_heights(cfg_.n_levels_);
    std::vector<float> level_scales_x(cfg_.n_levels_);
    std::vector<float> level_scales_y(cfg_.n_levels_);

		level_widths[0] = width;
		level_heights[0] = height;
		level_scales_x[0] = 1.0f;
		level_scales_y[0] = 1.0f;

    for (int level = 1; level < cfg_.n_levels_; ++level) {
			level_widths[level] = std::ceil(level_widths[level - 1] / cfg_.downfactor_);
      level_heights[level] = std::ceil(level_heights[level - 1] / cfg_.downfactor_);
			level_scales_x[level] = level_scales_x[level - 1] * level_widths[level - 1] / level_widths[level];
			level_scales_y[level] = level_scales_y[level - 1] * level_heights[level - 1] / level_heights[level];
		}

    for (int level = 0; level < cfg_.n_levels_; ++level) {
			float_buffer_.height_ = level_heights[level];
			float_buffer_.width_ = level_widths[level];
			keypoint_flag_.height_ = level_heights[level];
			keypoint_flag_.width_ = level_widths[level];
			keypoint_indices_.height_ = level_heights[level];
			keypoint_indices_.width_ = level_widths[level];
			compact_indices_.height_ = level_heights[level];
			compact_indices_.width_ = level_widths[level];
			sorted_compact_indices_.height_ = level_heights[level];
			sorted_compact_indices_.width_ = level_widths[level];
			compact_score_.height_ = level_heights[level];
			compact_score_.width_ = level_widths[level];
			sorted_compact_score_.height_ = level_heights[level];
			sorted_compact_score_.width_ = level_widths[level];
			smooth_buffer_.height_ = level_heights[level];
			smooth_buffer_.width_ = level_widths[level];

			dim3 blocks(16, 16);
			dim3 grids(iDivUp(image_buffer_.height_, blocks.x), iDivUp(image_buffer_.width_, blocks.y));
			cudaResourceDesc resDesc;
			cudaTextureDesc texDesc;
			cudaTextureObject_t image_tex;

			// 0. downsample to nevel level
      if (level < cfg_.n_levels_ - 1) {
				low_res_image_buffer_.height_ = level_heights[level + 1];
				low_res_image_buffer_.width_ = level_widths[level + 1];

				std::memset(&resDesc, 0, sizeof(resDesc));
				std::memset(&texDesc, 0, sizeof(texDesc));
				resDesc.resType = cudaResourceTypePitch2D;
				resDesc.res.pitch2D.devPtr = image_buffer_.buffer_;
				resDesc.res.pitch2D.height = image_buffer_.height_;
				resDesc.res.pitch2D.width = image_buffer_.width_;
				resDesc.res.pitch2D.pitchInBytes = image_buffer_.pitch_;
				resDesc.res.pitch2D.desc = cudaCreateChannelDesc(8, 0, 0, 0, cudaChannelFormatKindUnsigned);
				texDesc.readMode = cudaReadModeNormalizedFloat;
				texDesc.filterMode = cudaFilterModeLinear;
				texDesc.normalizedCoords = true;

				CUDA_CALL(cudaCreateTextureObject(&image_tex, &resDesc, &texDesc, nullptr));
				DownsampleKernel << < grids, blocks, 0, stream_ >> > (image_tex, low_res_image_buffer_);
				CUDA_CALL(cudaDestroyTextureObject(image_tex));
			}

			// 1. detect kpt on un-smoothed image pyramid level
			std::memset(&resDesc, 0, sizeof(resDesc));
			std::memset(&texDesc, 0, sizeof(texDesc));
			resDesc.resType = cudaResourceTypePitch2D;
			resDesc.res.pitch2D.devPtr = image_buffer_.buffer_;
			resDesc.res.pitch2D.height = image_buffer_.height_;
			resDesc.res.pitch2D.width = image_buffer_.width_;
			resDesc.res.pitch2D.pitchInBytes = image_buffer_.pitch_;
			resDesc.res.pitch2D.desc = cudaCreateChannelDesc(8, 0, 0, 0, cudaChannelFormatKindUnsigned);
			texDesc.readMode = cudaReadModeElementType;
			texDesc.filterMode = cudaFilterModePoint;

			CUDA_CALL(cudaCreateTextureObject(&image_tex, &resDesc, &texDesc, nullptr));
      Fast9CornerDetectionKernel << <grids, blocks, 0, stream_ >> >(image_tex, cfg_.fast_threshold_, cfg_.border_threshold_, float_buffer_);
			CUDA_CALL(cudaDestroyTextureObject(image_tex));

			std::memset(&resDesc, 0, sizeof(resDesc));
			std::memset(&texDesc, 0, sizeof(texDesc));
			resDesc.resType = cudaResourceTypePitch2D;
			resDesc.res.pitch2D.devPtr = float_buffer_.buffer_;
			resDesc.res.pitch2D.height = float_buffer_.height_;
			resDesc.res.pitch2D.width = float_buffer_.width_;
			resDesc.res.pitch2D.pitchInBytes = float_buffer_.pitch_;
			resDesc.res.pitch2D.desc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
			texDesc.readMode = cudaReadModeElementType;
			texDesc.filterMode = cudaFilterModePoint;

			cudaTextureObject_t fp_score_tex;
			CUDA_CALL(cudaCreateTextureObject(&fp_score_tex, &resDesc, &texDesc, nullptr));
			NonMaximumSuppressionKernel << <grids, blocks, 0, stream_ >> >
        (fp_score_tex, keypoint_flag_, cfg_.border_threshold_);
			CUDA_CALL(cudaDestroyTextureObject(fp_score_tex));

			CUDA_CALL(cub::DeviceScan::InclusiveSum(d_prescan_buffer_, d_prescan_buffer_size_,
				keypoint_flag_.buffer_, keypoint_indices_.buffer_,
				keypoint_indices_.height_ * keypoint_indices_.width_, stream_));

			CompactKeypointsKernel << <grids, blocks, 0, stream_ >> >(compact_indices_, keypoint_indices_, keypoint_flag_, d_feature_number_);
			CUDA_CALL(cudaMemcpyAsync(h_feature_number_, d_feature_number_, sizeof(unsigned int), cudaMemcpyDeviceToHost, stream_));
			CUDA_CALL(cudaStreamSynchronize(stream_));

			std::memset(&resDesc, 0, sizeof(resDesc));
			std::memset(&texDesc, 0, sizeof(texDesc));
			resDesc.resType = cudaResourceTypePitch2D;
			resDesc.res.pitch2D.devPtr = image_buffer_.buffer_;
			resDesc.res.pitch2D.height = image_buffer_.height_;
			resDesc.res.pitch2D.width = image_buffer_.width_;
			resDesc.res.pitch2D.pitchInBytes = image_buffer_.pitch_;
			resDesc.res.pitch2D.desc = cudaCreateChannelDesc(8, 0, 0, 0, cudaChannelFormatKindUnsigned);
			texDesc.readMode = cudaReadModeNormalizedFloat;
			texDesc.filterMode = cudaFilterModeLinear;

			CUDA_CALL(cudaCreateTextureObject(&image_tex, &resDesc, &texDesc, nullptr));
			int threadsPerBlock = 256;
			int blocksPerGrid = iDivUp(*h_feature_number_, threadsPerBlock);
			// compute harris corner response if necessary
			if (*h_feature_number_ > h_octave_max_feature_numbers_[level]) {
				ComputeHarrisScorenessKernel << <blocksPerGrid, threadsPerBlock, 0, stream_ >> >(image_tex, compact_indices_,
          compact_score_.buffer_, *h_feature_number_, cfg_.harris_threshold_);
				CUDA_CALL(cub::DeviceRadixSort::SortPairsDescending(d_sort_buffer_, d_sort_buffer_size_,
					compact_score_.buffer_,
					sorted_compact_score_.buffer_,
					compact_indices_.buffer_,
					sorted_compact_indices_.buffer_,
					*h_feature_number_,
					0, sizeof(float) * 8, stream_));
				if (!upright) {
					blocksPerGrid = iDivUp(h_octave_max_feature_numbers_[level], threadsPerBlock);
					ComputeOrientation << <blocksPerGrid, threadsPerBlock, 0, stream_ >> >(image_tex, sorted_compact_indices_, d_keypoints_, *h_feature_number_, level_scales_x[level], level_scales_y[level]);
				}
			} else if (*h_feature_number_ > 0 && !upright) {
				ComputeOrientation << <blocksPerGrid, threadsPerBlock, 0, stream_ >> >(image_tex, compact_indices_, d_keypoints_, *h_feature_number_, level_scales_x[level], level_scales_y[level]);
			}

			CUDA_CALL(cudaDestroyTextureObject(image_tex));

			// 3. smooth
			std::memset(&resDesc, 0, sizeof(resDesc));
			std::memset(&texDesc, 0, sizeof(texDesc));
			resDesc.resType = cudaResourceTypePitch2D;
			resDesc.res.pitch2D.devPtr = image_buffer_.buffer_;
			resDesc.res.pitch2D.height = image_buffer_.height_;
			resDesc.res.pitch2D.width = image_buffer_.width_;
			resDesc.res.pitch2D.pitchInBytes = image_buffer_.pitch_;
			resDesc.res.pitch2D.desc = cudaCreateChannelDesc(8, 0, 0, 0, cudaChannelFormatKindUnsigned);
			texDesc.readMode = cudaReadModeNormalizedFloat;
			texDesc.filterMode = cudaFilterModePoint;

			CUDA_CALL(cudaCreateTextureObject(&image_tex, &resDesc, &texDesc, nullptr));
			GaussianBlurRowKernel << < grids, blocks, 0, stream_ >> > (image_tex, float_buffer_);
			CUDA_CALL(cudaDestroyTextureObject(image_tex));

			std::memset(&resDesc, 0, sizeof(resDesc));
			std::memset(&texDesc, 0, sizeof(texDesc));
			resDesc.resType = cudaResourceTypePitch2D;
			resDesc.res.pitch2D.devPtr = float_buffer_.buffer_;
			resDesc.res.pitch2D.height = float_buffer_.height_;
			resDesc.res.pitch2D.width = float_buffer_.width_;
			resDesc.res.pitch2D.pitchInBytes = float_buffer_.pitch_;
			resDesc.res.pitch2D.desc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
			texDesc.readMode = cudaReadModeElementType;
			texDesc.filterMode = cudaFilterModePoint;

			cudaTextureObject_t fp_tex;
			CUDA_CALL(cudaCreateTextureObject(&fp_tex, &resDesc, &texDesc, nullptr));
			GaussianBlurColKernel << <grids, blocks, 0, stream_ >> >(fp_tex, smooth_buffer_);
			CUDA_CALL(cudaDestroyTextureObject(fp_tex));

			// 4. descriptors
			std::memset(&resDesc, 0, sizeof(resDesc));
			std::memset(&texDesc, 0, sizeof(texDesc));
			resDesc.resType = cudaResourceTypePitch2D;
			resDesc.res.pitch2D.devPtr = smooth_buffer_.buffer_;
			resDesc.res.pitch2D.height = smooth_buffer_.height_;
			resDesc.res.pitch2D.width = smooth_buffer_.width_;
			resDesc.res.pitch2D.pitchInBytes = smooth_buffer_.pitch_;
			resDesc.res.pitch2D.desc = cudaCreateChannelDesc(8, 0, 0, 0, cudaChannelFormatKindUnsigned);
			texDesc.readMode = cudaReadModeNormalizedFloat;
			texDesc.filterMode = cudaFilterModeLinear;
			CUDA_CALL(cudaCreateTextureObject(&image_tex, &resDesc, &texDesc, nullptr));

			if (*h_feature_number_ > h_octave_max_feature_numbers_[level]) {
				// already sorted
				*h_feature_number_ = h_octave_max_feature_numbers_[level];
				blocksPerGrid = iDivUp(*h_feature_number_, threadsPerBlock);
				if (upright) {
					ExtractOrbDescriptorUpright << <blocksPerGrid, threadsPerBlock, 0, stream_ >> >
            (image_tex, proj_matrix_tex_, sorted_compact_indices_, d_keypoints_, d_descriptors_, d_descriptors_embedding_, *h_feature_number_, cfg_.embedding_dimension_, level_scales_x[level], level_scales_y[level]);
				} else {
          ExtractOrbDescriptor << <blocksPerGrid, threadsPerBlock, 0, stream_ >> >(image_tex, proj_matrix_tex_, sorted_compact_indices_, d_keypoints_, d_descriptors_, d_descriptors_embedding_, *h_feature_number_, cfg_.embedding_dimension_);
				}
			} else if (*h_feature_number_ > 0) {
				threadsPerBlock = 256;
				blocksPerGrid = iDivUp(*h_feature_number_, threadsPerBlock);

				if (upright) {
					ExtractOrbDescriptorUpright << <blocksPerGrid, threadsPerBlock, 0, stream_ >> >
            (image_tex, proj_matrix_tex_, compact_indices_, d_keypoints_, d_descriptors_, d_descriptors_embedding_, *h_feature_number_, cfg_.embedding_dimension_, level_scales_x[level], level_scales_y[level]);
				} else {
          ExtractOrbDescriptor << <blocksPerGrid, threadsPerBlock, 0, stream_ >> >(image_tex, proj_matrix_tex_, compact_indices_, d_keypoints_, d_descriptors_, d_descriptors_embedding_, *h_feature_number_, cfg_.embedding_dimension_);
				}
			}
			CUDA_CALL(cudaMemcpyAsync(h_pinned_kpts_ + h_feature_offset_, d_keypoints_,
				sizeof(float4) * (*h_feature_number_), cudaMemcpyDeviceToHost, stream_));
      CUDA_CALL(cudaMemcpyAsync(h_pinned_desc_ + h_feature_offset_ * 32, d_descriptors_,
        32 * (*h_feature_number_), cudaMemcpyDeviceToHost, stream_));
      CUDA_CALL(cudaMemcpyAsync(h_descriptors_embedding_ + h_feature_offset_ * cfg_.embedding_dimension_, d_descriptors_embedding_,
        sizeof(float) * cfg_.embedding_dimension_ * (*h_feature_number_), cudaMemcpyDeviceToHost, stream_));

			h_feature_offset_ += *h_feature_number_;
			CUDA_CALL(cudaDestroyTextureObject(image_tex));

			std::swap(image_buffer_, low_res_image_buffer_);
		}
	}

	int OrbCUDA::Download(float *h_kpts, uint8_t *h_desc, float* h_desc_embedding) {
		// last synchronize before copy out
		CUDA_CALL(cudaStreamSynchronize(stream_));
		std::memcpy(h_kpts, h_pinned_kpts_, sizeof(float4) * h_feature_offset_);
    std::memcpy(h_desc, h_pinned_desc_, sizeof(uint8_t) * 32 * h_feature_offset_);
    std::memcpy(h_desc_embedding, h_descriptors_embedding_, sizeof(float) * cfg_.embedding_dimension_ * h_feature_offset_);
		return h_feature_offset_;
	}

	bool OrbCUDA::Init() {
		CUDA_CALL(cudaSetDevice(cfg_.gpu_id_));
		CUDA_CALL(cudaStreamCreate(&stream_));
		image_buffer_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);
    low_res_image_buffer_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);
    smooth_buffer_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);
    float_buffer_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);
    proj_matrix_.Allocate(256, 256);
    keypoint_flag_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);
    keypoint_indices_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);
    compact_indices_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);
    sorted_compact_indices_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);
    compact_score_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);
    sorted_compact_score_.Allocate(cfg_.maximum_resolution_, cfg_.maximum_resolution_);

		CUDA_CALL(cub::DeviceScan::InclusiveSum(d_prescan_buffer_, d_prescan_buffer_size_, keypoint_flag_.buffer_,
			keypoint_indices_.buffer_, keypoint_flag_.height_ * keypoint_flag_.width_, stream_));
		CUDA_CALL(cudaMalloc(&d_prescan_buffer_, d_prescan_buffer_size_));
		CUDA_CALL(cub::DeviceRadixSort::SortPairsDescending(d_sort_buffer_, d_sort_buffer_size_,
			compact_score_.buffer_,
			sorted_compact_score_.buffer_, // keys
			compact_indices_.buffer_,
			sorted_compact_indices_.buffer_, // values
			cfg_.maximum_resolution_ *
			cfg_.maximum_resolution_, // num items
			0, sizeof(float) * 8, // bits
			stream_));
		CUDA_CALL(cudaMalloc(&d_sort_buffer_, d_sort_buffer_size_));
		CUDA_CALL(cudaMalloc(&d_feature_number_, sizeof(int)));
		CUDA_CALL(cudaMalloc(&d_keypoints_, sizeof(float4) * cfg_.maximum_feature_num_));
    CUDA_CALL(cudaMalloc(&d_descriptors_, 256 / 8 * cfg_.maximum_feature_num_));
    CUDA_CALL(cudaMalloc(&d_descriptors_embedding_, sizeof(float) * cfg_.embedding_dimension_ * cfg_.maximum_feature_num_));
		CUDA_CALL(cudaMallocHost(&h_pinned_kpts_, sizeof(float4) * cfg_.maximum_feature_num_));
    CUDA_CALL(cudaMallocHost(&h_pinned_desc_, 256 / 8 * cfg_.maximum_feature_num_));
    CUDA_CALL(cudaMallocHost(&h_descriptors_embedding_, sizeof(float) * cfg_.embedding_dimension_ * cfg_.maximum_feature_num_));
		CUDA_CALL(cudaMallocHost(&h_pinned_buffer_,
			sizeof(unsigned char) * cfg_.maximum_resolution_ * cfg_.maximum_resolution_));
		CUDA_CALL(cudaMallocHost(&h_feature_number_, sizeof(int)));

		h_feature_offset_ = 0;

		// initialize the fast LUT
		unsigned char threshold_tab[512]{};
		for (int i = -255; i <= 255; i++) {
			threshold_tab[i + 255] = (unsigned char)(i < -cfg_.fast_threshold_ ? 1 : i > cfg_.fast_threshold_ ? 2 : 0);
		}
		CUDA_CALL(cudaMemcpyToSymbolAsync(fast_corner_compare_lut, threshold_tab, sizeof(unsigned char) * 512, 0,
			cudaMemcpyHostToDevice, stream_));

		h_octave_max_feature_numbers_.resize(cfg_.n_levels_);
		float factor = 1.0f / cfg_.downfactor_;
		float n_desired_features_per_scale =
      cfg_.maximum_feature_num_ * (1 - factor) / (1 - (float)std::pow((double)factor, (double)cfg_.n_levels_));
		unsigned int sumFeatures = 0;
    for (int level = 0; level < cfg_.n_levels_ - 1; ++level) {
			h_octave_max_feature_numbers_[level] = static_cast<unsigned int>(n_desired_features_per_scale + 0.5f);
			sumFeatures += h_octave_max_feature_numbers_[level];
			n_desired_features_per_scale *= factor;
		}
    h_octave_max_feature_numbers_[cfg_.n_levels_ - 1] = std::max(cfg_.maximum_feature_num_ - sumFeatures, 0U);

    /////////////////////////////////
    // Initialize projection matrix.
    proj_matrix_.Upload(proj_matrix_data, 256, 256, stream_);
    cudaResourceDesc resDesc;
    cudaTextureDesc texDesc;
    std::memset(&resDesc, 0, sizeof(resDesc));
    std::memset(&texDesc, 0, sizeof(texDesc));
    resDesc.resType = cudaResourceTypePitch2D;
    resDesc.res.pitch2D.devPtr = proj_matrix_.buffer_;
    resDesc.res.pitch2D.height = proj_matrix_.height_;
    resDesc.res.pitch2D.width = proj_matrix_.width_;
    resDesc.res.pitch2D.pitchInBytes = proj_matrix_.pitch_;
    resDesc.res.pitch2D.desc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
    texDesc.readMode = cudaReadModeElementType;
    texDesc.filterMode = cudaFilterModePoint;
    texDesc.normalizedCoords = false;
    CUDA_CALL(cudaCreateTextureObject(&proj_matrix_tex_, &resDesc, &texDesc, nullptr));

		return true;
	}
} // namespace chitu